/*
vue router 官方文档
https://router.vuejs.org/zh/guide/
 */

import { createRouter, createWebHashHistory } from 'vue-router'

// 组件
import Empty from '../components/Empty.vue'
import HelloWorld from '../components/HelloWorld.vue'

// 路由
const routes = [
  {path: '/', component: Empty},
  {path: '/hello', component: HelloWorld},
]

export const router = createRouter({
  history: createWebHashHistory(),
  routes,
})
