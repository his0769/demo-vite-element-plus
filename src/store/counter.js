import { defineStore } from 'pinia'
import { ref } from 'vue'

// 示例代码参考： https://pinia.vuejs.org/zh/introduction.html#basic-example
// 使用组合API:  https://pinia.vuejs.org/zh/core-concepts/#setup-stores
export const useCounterStore = defineStore('counter', () => {
  const counter = ref(0)
  function increment() {
    counter.value++
  }

  return { counter, increment }
})
