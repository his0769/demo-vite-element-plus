
# 2024年2月4日
- （2024年2月4日01点09分）做了很多`vite`练习，但是没有停下来总结，用时又从头开始，这样很没有效率！
- 所以，这次，再创建一个项目把之前的资料再次整理一番；（不断整理，复习，再整理，总能到得到最完善的一版）
- <https://www.jianshu.com/p/7108e1291c0e>
  - 这篇文档，可以作为有价值的参考；
- (240909) 将`tailwindcss`添加到`vite`中
- end

# 创建`vue vite`项目

```sh
# 参考下面的文档
# https://www.jianshu.com/p/7108e1291c0e

pnpm create vite demo-vite-element-plus --template vue

  cd demo-vite-element-plus
  pnpm install
  pnpm run dev

pnpm add element-plus vue-router@4 pinia axios

pnpm add -D sass
```

# element-plus自动导入

> 可以直接复制下面的 `vite.config.js` 的代码

```sh
pnpm add -D unplugin-vue-components unplugin-auto-import
```

```js
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// ...
    // },
    plugins: [
      vue(),
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  // })
// }
```

# `vite.config.js`

```js
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

const PORT = 5173;

// https://vitejs.dev/config/
export default ({ mode }) => {
  let BASE = "/"
  if (mode === "production") {
    // 可以执行一些不同的配置，比如： BASE = "/public"
  }
  return defineConfig({
    base: BASE,
    server: {
      port: PORT
    },
    css: {
      preprocessorOptions: {
        scss: {
          // additionalData: `@use "~/styles/element/index.scss" as *;`,
        },
      },
    },
    plugins: [
      vue(),
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  })
}
```

- 去除了 '~/': `${pathSrc}/`
  - 这点和 <https://www.jianshu.com/p/7108e1291c0e> 不同；
- end

# Empty & HelloWorld

- `HelloWorld` 是 create 时，自动生成的
- `Empty` ，可以创建一个，如下：

```vue
<script setup>
</script>

<template>
  <div></div>
</template>

<style scoped>
</style>
```

- 提供一个 `Empty` 组件 ，让示例代码更完整； 路由更合理；否则，可能会出现错误 `<router-view></router-view>` 提示找不到 `/` ；

# router

- `src/router/index.js`
- 首先，应该创建这个文件，修改 `main.js` 时，可以引用“路由”；

```js
/*
vue router 官方文档
https://router.vuejs.org/zh/guide/
 */

import { createRouter, createWebHashHistory } from 'vue-router'

// 组件
import HelloWorld from '../components/HelloWorld.vue'
import Empty from '../components/Empty.vue'

// 路由
const routes = [
  {path: '/', component: Empty},
  {path: '/hello', component: HelloWorld},
]

export const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

```

# `router.push`

```js
import { useRouter } from 'vue-router'
const router = useRouter()

// 导航
function navigateTo(url) {
  router.push(url)
  changeShow(false)
}
```


# `main.js`

```js
import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'

import { createPinia } from 'pinia'
import { router } from './router/index'

const app = createApp(App)

app.use(router)
app.use(createPinia())
app.mount('#app')

```

- 路由和pinia都有包含；

# `App.vue`

- `App.vue` 中放入 `<router-view></router-view>` ，这样路由才有地方可以渲染；

```vue
<router-view></router-view>
```

# pinia

- pinia 的编写示例参考下面代码

```js
import { defineStore } from 'pinia'
import { ref } from 'vue'

// 示例代码参考： https://pinia.vuejs.org/zh/introduction.html#basic-example
// 使用组合API:  https://pinia.vuejs.org/zh/core-concepts/#setup-stores
export const useCounterStore = defineStore('counter', () => {
  const counter = ref(0)
  function increment() {
    counter.value++
  }

  return { counter, increment }
})
```

- pinia 的引用关键参考下面代码

```js
import { useCounterStore } from '../store/counter'
const counterStore = useCounterStore()
import { storeToRefs } from 'pinia'
const { counter } = storeToRefs(counterStore)
const { increment } = counterStore
```

- [./src/store/counter.js](./src/store/counter.js) ， pinia示例 （store编写示例）
- [./src/components/HelloWorld.vue](./src/components/HelloWorld.vue) ， pinia引用示例（store使用示例）
- end

# ~~vite打包时排除某些依赖~~

> 一般情况下不需要排除依赖

咨询AI `vite如何在打包时排除某些依赖？` : [./docs/d101_vite如何在打包时排除某些依赖_notes.md](./docs/d101_vite如何在打包时排除某些依赖_notes.md) ， 需要 `vite-plugin-html` 依赖，帮助修改 `dist/index.html`

```sh
pnpm add -D vite-plugin-html
```

vite.config.js

```js
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

import { createHtmlPlugin } from 'vite-plugin-html'

const PORT = 5173;

// https://vitejs.dev/config/
export default ({ mode }) => {
  let BASE = "/"
  if (mode === "production") {
    // 可以执行一些不同的配置，比如： BASE = "/public"
  }
  return defineConfig({
    base: BASE,
    server: {
      port: PORT
    },
    css: {
      preprocessorOptions: {
        scss: {
          // additionalData: `@use "~/styles/element/index.scss" as *;`,
        },
      },
    },
    plugins: [
      vue(),
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
      createHtmlPlugin(),
    ],
    build: {
      rollupOptions: {
        output: {
          manualChunks: {
            // 将特定模块打包到特定chunk
            'echarts': ['echarts'],
            'element-plus': ['element-plus'],
          },
        },
      },
    },
  })
}
```

关键是下面这段“配置”

```js
plugins: [
  // ...
  createHtmlPlugin(),
],
build: {
  rollupOptions: {
    output: {
      manualChunks: {
        // 将特定模块打包到特定chunk
        'echarts': ['echarts'],
        'element-plus': ['element-plus'],
      },
    },
  },
},
```

**【注意】** `'echarts': ['echarts'],` 中，数组（`['echarts']`）中的 `echarts` 才是模块的名字，不要写错； 比如 `'react-vendor': ['react', 'react-dom'],` 是将 `react` 和 `react-dom` 打包到 `react-vendor` 中；


# 其他

- 其他的没有列出的参考 <https://www.jianshu.com/p/7108e1291c0e>
- end

# tailwindcss(240909)

- 参考 <https://www.tailwindcss.cn/docs/guides/vite#vue>


```sh
npm create vite@latest my-project -- --template vue
cd my-project

# npm install -D tailwindcss postcss autoprefixer
pnpm add -D tailwindcss postcss autoprefixer
npx tailwindcss init -p

# tailwind.config.js

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

# style.css

@tailwind base;
@tailwind components;
@tailwind utilities;

npm run dev

# App.vue

<template>
  <h1 class="text-3xl font-bold underline">
    Hello world!
  </h1>
</template>
```




