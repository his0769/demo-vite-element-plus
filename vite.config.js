import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

const PORT = 5173;

// https://vitejs.dev/config/
export default ({ mode }) => {
  let BASE = "/"
  if (mode === "production") {
    // 可以执行一些不同的配置，比如： BASE = "/public"
  }
  return defineConfig({
    base: BASE,
    server: {
      port: PORT
    },
    css: {
      preprocessorOptions: {
        scss: {
          // additionalData: `@use "~/styles/element/index.scss" as *;`,
        },
      },
    },
    plugins: [
      vue(),
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  })
}